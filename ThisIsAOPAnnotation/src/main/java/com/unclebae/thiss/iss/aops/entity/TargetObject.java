package com.unclebae.thiss.iss.aops.entity;

import org.springframework.stereotype.Component;

@Component
public class TargetObject {

	public void printName() {
		String name = "KIDO";
		System.out.println("My name is " + name);
	}
	
	public void printAge() {
		int age = 20;
		System.out.println(String.format("I'm %d years old.", age));
	}
	
	public String printReturnMethod(String ... names) {
		String returnSTring = "This is AOP world.";
		
		String join = "";
		if (names != null && names.length > 0) {
			join = String.join(",", names);
		}
		System.out.println("Return Method is : " + join);
		
		return returnSTring;
	}
	
	public void printException() {
		System.out.println("printException ...");
		int exception = 1 / 0;
		System.out.println(exception);
	}
}
