package com.unclebae.thiss.iss.aops.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAspect {

	@Pointcut("execution(* com.unclebae.thiss.iss.aops.entity.*.*(..))")
	private void allComponents() {}
	
	@Before("allComponents()")
	public void beforeAdvice() {
		System.out.println("------------ Before Advice");
	}
	
	@After("allComponents()")
	public void afterAdvice() {
		System.out.println("=========== After Advice");
	}
	
	@AfterReturning(pointcut = "allComponents()", returning="retVal")
	public void afterReturningAdvice(Object retVal) {
		System.out.println("-.-.-.-.-.-.- After Returning " + retVal);
	}
	
	@AfterThrowing(pointcut = "allComponents()", throwing="ex")
	public void AfterThrowingAdvice(Exception ex) {
		System.out.println("=.=.=.=.=.=.=.= After Throwing " + ex.getMessage());
	}
}
